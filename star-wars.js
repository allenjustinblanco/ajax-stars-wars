let request = $.ajax("https://swapi.dev/api/people/")

request.done(function(data){
    let html = constructHtml(data.results)
})

// $.ajax("https://swapi.dev/api/people/").done(function(data){
//     console.log(data.results)
// })

// $.get("https://swapi.dev/api/people/").done(function(data){
//     console.log(data.results)
// })

function constructHtml(characters) {

    let characterHTML = "";

    characters.forEach(function(character){
        
        $.ajax(character.homeworld).done(function(data) {
            let homeworld = data.name;
            let films = '';
            character.films.forEach(function(filmURL) {
                $.ajax(filmURL).done(function(data){
                    console.log(data.title)
                    films += `${data.title}, `;

                    characterHTML += `
                        <tr>
                            <td>${character.name}</td>
                            <td>${character.height}</td>
                            <td>${character.mass}</td>
                            <td>${character.birth_year}</td>
                            <td>${character.gender}</td>
                            <td>${homeworld}</td>
                            <td>${films}</td>
                        </tr>
                    `
                })
            })
        })

    })

    setTimeout( function() {
        $('#characters').fadeIn()
        $('#insertCharacters').html(characterHTML)
    }, 2000)

   
    
}